package main

//#include <stdio.h>
import (
	"C"
	"math/big"
)
import "fmt"

//LCG Linear generator struct

type LCG struct {
	Seed          uint64
	Multiplicator uint64
	Incrementer   uint64
	Modulus       uint64
}

//GenerateOutput Generating LCG output
func (l LCG) GenerateOutput() uint64 {
	l.Seed = (l.Seed*l.Multiplicator + l.Incrementer) % l.Modulus
	return l.Seed

}
func main() {
	x := big.NewInt(25)
	y := x.ModInverse(x, big.NewInt(20))
	fmt.Println(y)
}
