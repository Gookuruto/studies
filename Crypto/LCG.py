from __future__ import division
from fractions import gcd
import sys
from collections import deque
import functools
import math
import numpy as np
import random


class prng_lcg:

    def __init__(self, seed,m=302080878814014441,c=3613230612905734352,n=4611686018427387847):
        self.state = seed  # the "seed"
        self.m = m
        self.n = n
        self.c = c

    def next(self):
        self.state = (self.state * self.m + self.c) % self.n
        return self.state


def test(test_count):
    rezultat = []
    modulus = [2**8-5,2**9-3,2**10-3,2**11-9,2**59-55,2**31-1,2**32]
    mult = [33,55,35,110,65,331,995,328,346764851511064641,124795884580648576,16807,48271,1664525,1664525]
    inc =[0,0,0,0,0,0,0,0,0,0,0,0,1013904223,1013904223]
    for i in range(len(mult)):
        seed = 123
        if (i+1)%2 !=0: 
            gen = prng_lcg(seed,mult[i],inc[i],modulus[int(np.floor(i/2))])  # seed = 123
        else:
            gen = prng_lcg(seed,mult[i],inc[i],modulus[int(np.floor((i-1)/2))])
        x=[]
        y=[]
        result=[]
        succes=0
        all=0
        for i in range(test_count):
            x.append(gen.next())
        y=x[0:int(len(x)/2)]
        del x[0:int(len(x)/2)]
        for i in range(len(x)):
            all+=1
            result.append(next_random_number(y))
            y.append(result[-1])
        for i in range(len(x)):
            if x[i]==result[i]:
                succes+=1
        temp = succes/all
        rezultat.append(temp)
    print(rezultat)
    return np.mean(rezultat)

def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, x, y = egcd(b % a, a)
        return (g, y - (b // a) * x, x)


def modinv(b, n):
    g, x, _ = egcd(b, n)
    if g == 1:
      return x % n


def crack_unknown_increment(states, modulus, multiplier):
    increment = (states[1] - states[0] * multiplier) % modulus
    return modulus, multiplier, increment


def crack_unknown_multiplier(states, modulus):
    multiplier = (states[2] - states[1]) * modinv(states[1] - states[0], modulus) % modulus
    return crack_unknown_increment(states, modulus, multiplier)


def crack_unknown_modulus(states):
    diffs = [s1 - s0 for s0, s1 in zip(states, states[1:])]
    zeroes = [t2 * t0 - t1 * t1 for t0, t1, t2 in zip(diffs, diffs[1:], diffs[2:])]
    modulus = abs(functools.reduce(math.gcd, zeroes))
    return crack_unknown_multiplier(states, modulus)

def coprime(a, b):
    return np.gcd(a, b) == 1

def next_random_number(states):
    while states[0]>states[1]:
        del states[0]
    last_number = states[-1]
    if(states[0]>=states[1]):
        return "to nie zadziala"
    m, a, c = crack_unknown_modulus(states)
    return (a * last_number + c) % m




print("probability of success is:  ")
print(test(100))
