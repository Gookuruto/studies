package main

import (
	"fmt"
	"math"
	"time"

	"gonum.org/v1/plot/vg"

	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"

	"gonum.org/v1/gonum/stat"

	"golang.org/x/exp/rand"
)

func randomChoice(val1, val2 int, probability_of_val1 float64) int {
	x := rand.Float64()
	if x < probability_of_val1 {
		return val1
	}
	return val2

}

func electionP(probabilities []float64, n int) (int, int) {
	var slots int
	var numberOfSlots int
	var round int = 1
	var k int
	if n == 0 {
		n = len(probabilities)
	}
	for slots != 1 {
		slots = 0
		numberOfSlots++
		for i := 0; i < n; i++ {
			if k >= len(probabilities) {
				k = 0
				round++
			}
			slots = slots + randomChoice(1, 0, probabilities[k])

		}
		k++

	}

	return numberOfSlots, round
}

func probabilityRoundOne(dataRound []float64) float64 {
	var sum float64
	for _, v := range dataRound {
		if v == 1 {
			sum++
		}
	}
	return sum / float64(len(dataRound))
}
func testElectionScenarioOne(numberTests, n int) ([]float64, []float64, float64, float64) {
	dataSlots := make([]float64, 0)
	dataRounds := make([]float64, 0)
	proabilities := make([]float64, 0)
	for i := 0; i < n; i++ {
		proabilities = append(proabilities, 1.0/float64(n))

	}
	for j := 0; j < numberTests; j++ {
		tmpS, tmpR := electionP(proabilities, n)
		dataSlots = append(dataSlots, float64(tmpS))
		dataRounds = append(dataRounds, float64(tmpR))
	}
	return dataSlots, dataRounds, stat.Mean(dataSlots, nil), stat.Variance(dataSlots, nil)
}
func testElection(testCount, n, u int) ([]float64, []float64, float64, float64) {
	dataSlots := make([]float64, 0)
	dataRounds := make([]float64, 0)
	probabilities := make([]float64, 0)

	for i := 0; i < int(math.Ceil(math.Log2(float64(n)))); i++ {
		probabilities = append(probabilities, 1.0/(math.Pow(2, float64(i+1))))

	}
	fmt.Println(probabilities)
	for j := 0; j < testCount; j++ {
		tmpS, tmpR := electionP(probabilities, u)
		dataSlots = append(dataSlots, float64(tmpS))
		dataRounds = append(dataRounds, float64(tmpR))

	}
	return dataSlots, dataRounds, stat.Mean(dataSlots, nil), stat.Variance(dataSlots, nil)

}

func generateHistogram(data []float64, xRange int, outputFile string) {
	v := make(plotter.Values, len(data))
	for i := range v {
		v[i] = data[i]
	}
	p, err := plot.New()
	if err != nil {
		panic(err)
	}
	p.Title.Text = outputFile
	h, err := plotter.NewHist(v, xRange)
	if err != nil {
		panic(err)
	}
	// Normalize the area under the histogram to
	// sum to one.
	//h.Normalize(1)
	p.Add(h)
	if err := p.Save(4*vg.Inch, 4*vg.Inch, outputFile); err != nil {
		panic(err)
	}

}

func main() {
	rand.Seed(uint64(time.Now().UTC().UnixNano()))
	slotsData, roundData, Mean, Var := testElectionScenarioOne(1000,1000)
	slotsData2, roundData2, Mean2, Var2 := testElection(1000, 1000, 2)
	slotsData3, roundData3, Mean3, Var3 := testElection(1000, 1000, 500)
	slotsData4, roundData4, Mean4, Var4 := testElection(1000, 1000, 1000)

	go generateHistogram(slotsData, 1000, "scenarioOne.png")
	go generateHistogram(slotsData2, 1000, "scenartoU2.png")
	go generateHistogram(slotsData3, 1000, "uHalfn.png")
	go generateHistogram(slotsData4, 1000, "uIsn.png")
	fmt.Println("Mean = ", Mean, " Variance = ", Var)
	fmt.Println("Mean2 = ", Mean2, " Variance2 = ", Var2)
	fmt.Println("Mean3 = ", Mean3, " Variance3 = ", Var3)
	fmt.Println("Mean4 = ", Mean4, " Variance4 = ", Var4)
	//fmt.Println(roundData)
	fmt.Println("Probability round one : ", probabilityRoundOne(roundData))
	fmt.Println("Probability round one 2 : ", probabilityRoundOne(roundData2))
	fmt.Println("Probability round one 3 : ", probabilityRoundOne(roundData3))
	fmt.Println("Probability round one 4 : ", probabilityRoundOne(roundData4))
	//fmt.Println(slotsData4)
	time.Sleep(time.Second * 5)
}
